import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ReactiveFormsModule } from '@angular/forms';
import { MatExpansionModule } from '@angular/material/expansion';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDialogModule} from '@angular/material/dialog';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { AppComponent } from './app.component';
import { IntroComponent } from './view/intro/intro.component';
import { TimelineComponent } from './view/timeline/timeline.component';
import { FormComponent } from './form/form.component';
import { MenueComponent } from './parts/menue/menue.component';
import { HttpClientModule,HttpClient} from '@angular/common/http';
import { ActionNodeService } from './services/action-node.service';
import { DialogModalsService } from './services/dialog-modals.service';
import { FooterComponent } from './parts/footer/footer.component';
import { CounterPipe } from './pipes/counter.pipe';
import { ControlComponent } from './parts/control/control.component';
import { TimelineNodeComponent } from './parts/timeline-node/timeline-node.component';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { MiniTimerComponent } from './parts/mini-timer/mini-timer.component';


import { MatTooltipModule } from '@angular/material/tooltip';
import { AcceptComponent } from './parts/dialogs/accept/accept.component';
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
@NgModule({
  declarations: [
    AppComponent,
    IntroComponent,
    TimelineComponent,
    FormComponent,
    MenueComponent,
    FooterComponent,
    AcceptComponent,
    CounterPipe,
    ControlComponent,
    TimelineNodeComponent,
    MiniTimerComponent
  ],
  imports: [
    TranslateModule.forRoot({
      defaultLanguage: 'fr',
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    BrowserModule,
    AppRoutingModule,
    MatTooltipModule,
    MatDialogModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatSelectModule
  ],
  entryComponents: [AcceptComponent],
  providers: [ActionNodeService,HttpClient, DialogModalsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
