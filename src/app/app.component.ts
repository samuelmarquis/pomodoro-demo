import { Component } from '@angular/core';
//import { MenueComponent } from './parts/menue/menue.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'pomodoro-demo';
}
